import * as React from 'react';
import {TextInput, Button, IconButton} from 'react-native-paper';
import {StyleSheet, ScrollView, View} from 'react-native';
import ProfileAvatarDetail from '../components/profile/ProfileAvatarDetail';
import Counter from '../components/profile/Counter';

const gearicon = require('../assets/gear.png');
const profile = require('../assets/watame.png');
const Profile = () => {
  let [email, setEmail] = React.useState('');
  let [phone, setPhone] = React.useState('');
  let [twitter, setTwitter] = React.useState('');
  let [facebook, setFacebook] = React.useState('');

  return (
    <ScrollView>
      <View style={style.headingContainer}>
        <View style={style.headingContent}>
          <ProfileAvatarDetail
            title="Ho Hoai San"
            subtitle="Hue, Viet Nam"
            imgSrc={profile}
          />
        </View>
        <View style={style.placeTopRight}>
          <IconButton
            icon={gearicon}
            color="#FFF"
            size={20}
            onPress={() => console.log('Pressed')}
          />
        </View>
        <View style={style.flexRowCenter}>
          <Counter number={453} unit="followers" bgColor="#5FD2EB" />
          <Counter number={873} unit="following" bgColor="#42B7FF" />
        </View>
      </View>

      <View style={{...style.container, marginVertical: 20}}>
        <TextInput
          style={style.inputLight}
          label="Email"
          value={email}
          onChangeText={(text) => setEmail(text)}
        />
        <TextInput
          style={style.inputLight}
          label="Phone"
          value={phone}
          onChangeText={(text) => setPhone(text)}
        />
        <TextInput
          style={style.inputLight}
          label="Twitter"
          value={twitter}
          onChangeText={(text) => setTwitter(text)}
        />
        <TextInput
          style={style.inputLight}
          label="Facebook"
          value={facebook}
          onChangeText={(text) => setFacebook(text)}
        />
      </View>
    </ScrollView>
  );
};

const style = StyleSheet.create({
  flexCenter: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  flexRowCenter: {
    flexDirection: 'row',
    width: '100%',
    flex: 1,
    // justifyContent: 'center',
    // alignContent: 'center',
    // backgroundColor: 'pink',
  },
  headingContainer: {
    backgroundColor: '#5ECAFB',
  },
  headingContent: {
    paddingVertical: 20,
  },
  headingText: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
  },
  subHeadingText: {
    color: 'gray',
    fontSize: 15,
    textAlign: 'center',
  },
  profileImage: {
    borderWidth: 2,
    borderColor: 'white',
    borderRadius: 50,
  },
  container: {
    paddingHorizontal: 15,
  },
  placeTopRight: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: 10,
  },
  inputLight: {
    backgroundColor: 'white',
    marginBottom: 10,
  },
});

export default Profile;
