/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  Text,
  Button,
  StyleSheet,
  Image,
  ImageBackground,
  TouchableNativeFeedback,
} from 'react-native';

const backgroundImage = require('../assets/background.png');
const Card = ({title = '', img, description = ''}) => {
  return (
    <TouchableNativeFeedback>
      <View
        style={{
          borderRadius: 10,
          overflow: 'hidden',
          width: '100%',
          height: 80,
        }}>
        <View
          style={{
            padding: 10,
            backgroundColor: 'rgba(196, 196, 196, 0.3)',
            width: '100%',
            height: '100%',
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View
              style={{
                height: '100%',
                aspectRatio: 1,
                backgroundColor: 'rgba(0,0,0,0.1)',
                marginRight: 20,
                borderRadius: 100,

                overflow: 'hidden',
              }}>
              <Image
                source={{
                  uri: img,
                  width: 100,
                  height: 100,
                }}
                style={{width: '100%', height: '100%'}}
              />
            </View>
            <View style={{flexShrink: 1}}>
              <Text style={{color: 'white', fontSize: 18}}>{title}</Text>
              <View>
                <Text
                  numberOfLines={2}
                  style={{
                    color: 'white',
                  }}>
                  {description}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </TouchableNativeFeedback>
  );
};

const GridCell = (WrappedComponent) => {
  return (props) => {
    return (
      <View
        style={{
          width: '100%',
          padding: 5,
        }}>
        <WrappedComponent {...props} />
      </View>
    );
  };
};

const CardWarped = GridCell(Card);
const ListView = (props) => {
  const listItem = [
    {
      title: 'Facebook',
      img: 'https://picsum.photos/100',
      description: 'Facebook is a social media platform',
    },
    {
      title: 'Messenger',
      img: 'https://picsum.photos/101',
      description:
        'Messenger is a social media platform mostly focus on texting between people',
    },
    {
      title: 'Twitter',
      img: 'https://picsum.photos/102',
      description: 'Twitter is a social media platform too',
    },
    {
      title: 'Code React Native',
      img: 'https://picsum.photos/103',
      description: 'Nothing in particullar',
    },
    {
      title: 'Not thing important',
      img: 'https://picsum.photos/104',
      description: 'Yes',
    },
  ];
  return (
    <ImageBackground
      source={backgroundImage}
      style={{
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
      }}>
      <ScrollView style={{backgroundColor: 'transfarent'}}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            flexWrap: 'wrap',
          }}>
          {listItem.map((item, index) => (
            <CardWarped {...item} key={index} />
          ))}
        </View>
      </ScrollView>
    </ImageBackground>
  );
};

export default ListView;
