import * as React from 'react';
import {Image, ScrollView, StyleSheet, View, SafeAreaView} from 'react-native';
import {TextInput, Button, Text, Title} from 'react-native-paper';
const style = require('../assets/style');
const RegisterView = () => {
  const [fullname, setFullname] = React.useState('');
  const [username, setUsername] = React.useState('');
  const [email, setEmail] = React.useState('hohoaisan@gmail.com');
  const [phone, setPhone] = React.useState('');
  const [password, setPassword] = React.useState('');
  return (
    <ScrollView style={style.container}>
      <Image
        source={require('../assets/watame.png')}
        style={style.headingImg}
      />
      <Title style={style.headingText}>Welcome</Title>

      <View style={style.formControls}>
        <Text style={style.formHelperText}>
          Signup to start your new journey
        </Text>
        <TextInput
          label="Full name"
          value={fullname}
          onChangeText={(text) => setFullname(text)}
          mode="outlined"
          style={style.formControls}
        />
        <TextInput
          label="Username"
          value={username}
          onChangeText={(text) => setUsername(text)}
          mode="outlined"
          style={style.formControls}
        />
        <TextInput
          label="Email"
          value={email}
          onChangeText={(text) => setEmail(text)}
          mode="outlined"
          style={style.formControls}
        />
        <TextInput
          label="Phone"
          value={phone}
          onChangeText={(text) => setPhone(text)}
          mode="outlined"
          style={style.formControls}
        />
        <TextInput
          label="Password"
          value={password}
          onChangeText={(text) => setPassword(text)}
          secureTextEntry={true}
          mode="outlined"
          style={style.formControls}
        />
        <Button style={style.formControls} mode="contained" uppercase={true}>
          Go
        </Button>
        <Button uppercase={true}>Already have an account? Login</Button>
      </View>
    </ScrollView>
  );
};

export default RegisterView;
