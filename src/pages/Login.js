import * as React from 'react';
import {Image, ScrollView, StyleSheet, View} from 'react-native';
import {TextInput, Button, Text, Title} from 'react-native-paper';
const style = require('../assets/style');
const LoginView = () => {
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  return (
    <ScrollView style={style.container}>
      <Image
        source={require('../assets/watame.png')}
        style={style.headingImg}
      />
      <Title style={style.headingText}>Hello there, welcome back</Title>

      <View style={style.formControls}>
        <Text style={style.formHelperText}>Signin to continue</Text>
        <TextInput
          label="Username"
          value={email}
          onChangeText={(text) => setEmail(text)}
          mode="outlined"
        />
        <TextInput
          label="Password"
          value={password}
          onChangeText={(text) => setPassword(text)}
          secureTextEntry={true}
          mode="outlined"
        />
        <Button uppercase={true} style={style.formControls}>
          Forget password?
        </Button>
        <Button mode="contained" uppercase={true}>
          Go
        </Button>
        <Button uppercase={true}>New user? Sign up</Button>
      </View>
    </ScrollView>
  );
};

export default LoginView;
