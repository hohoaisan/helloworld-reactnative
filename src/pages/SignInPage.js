import React, {useState} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  ImageBackground,
  Image,
  SafeAreaView,
  TextInput,
  TouchableNativeFeedback,
} from 'react-native';

// Within your render function

const profile = require('../assets/watame.png');
const bgImg = require('../assets/background.png');
const icon = require('../assets/gear.png');
import CustomTextInput from '../components/CustomTextInput';
import CustomButtonGradient from '../components/CustomButtonGradient';

const SignInPage = (props) => {
  let [email, setEmail] = useState('');
  let [password, setPassword] = useState('');
  return (
    // <ImageBackground source={bgImg} style={{width: '100%', height: '100%'}}>
    <SafeAreaView style={{backgroundColor: 'white', flex: 1}}>
      <ScrollView style={{flex: 1}}>
        <View
          style={{alignItems: 'center', justifyContent: 'center', padding: 50}}>
          <Image source={profile} style={{height: 100, aspectRatio: 1}} />
        </View>
        <View style={{paddingHorizontal: 20, flex: 1}}>
          <Text style={{fontSize: 20, paddingVertical: 20, color: 'gray'}}>
            Sign in
          </Text>
          <CustomTextInput
            icon={icon}
            label="Email"
            onChangeText={(text) => {
              setEmail(text);
            }}
            style={{marginVertical: 10}}
          />
          <CustomTextInput
            icon={icon}
            label="Password"
            onChangeText={(text) => {
              setPassword(text);
            }}
            type="password"
            style={{marginVertical: 10}}
          />
          <View>
            <View>
              <CustomButtonGradient style={{flex: 1, marginVertical: 10}}>
                Signin to Facebook
              </CustomButtonGradient>
            </View>
            <View style={{flexDirection: 'row'}}>
              <CustomButtonGradient
                style={{flex: 1, marginVertical: 10, marginRight: 10}}
                onPress={() => console.log('hi')}
                colors={['#f95162', '#f95162']}>
                Signin
              </CustomButtonGradient>
              <CustomButtonGradient
                style={{flex: 1, marginVertical: 10, marginLeft: 10}}
                colors={['#1c68b6', '#1c68b6']}>
                Signup
              </CustomButtonGradient>
            </View>
          </View>
          <View style={{padding: 20}}>
            <Text
              style={{
                textAlign: 'center',
                color: '#f2b04d',
                fontWeight: 'bold',
                padding: 5,
              }}>
              Forgot password?
            </Text>
            <Text
              style={{
                textAlign: 'center',
                color: 'gray',
                fontWeight: 'bold',
                padding: 5,
              }}>
              Don't have account? Signup now
            </Text>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
    // </ImageBackground>
  );
};

export default SignInPage;
