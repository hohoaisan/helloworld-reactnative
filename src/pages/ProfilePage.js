import React, {useState} from 'react';
import {View, Text, SafeAreaView, ScrollView, Image} from 'react-native';
import PostCard from '../components/PostCard';
const profile = require('../assets/watame.png');
const ProfilePage = () => {
  return (
    <SafeAreaView style={{backgroundColor: 'white', flex: 1}}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingHorizontal: 15,
          position: 'absolute',
          top: 0,
          zIndex: 1,
          width: '100%',
          paddingVertical: 20,
          alignItems: 'center',
        }}>
        <Text style={{color: 'white', fontSize: 15}}>Settings</Text>
        <Text style={{color: 'white', fontSize: 20, fontWeight: 'bold'}}>
          Profile
        </Text>
        <Text style={{color: 'white', fontSize: 15}}>Logout</Text>
      </View>
      <ScrollView style={{flex: 1}}>
        <View>
          <View>
            <View style={{backgroundColor: '#5fb075', marginBottom: 50}}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingVertical: 20,
                }}>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: 160,
                    height: 160,
                    backgroundColor: 'white',
                    borderRadius: 200,
                    overflow: 'hidden',
                    bottom: -60,
                  }}>
                  <Image
                    source={profile}
                    style={{width: 150, height: 150, borderRadius: 200}}
                  />
                </View>
              </View>
            </View>
            <View>
              <Text
                style={{textAlign: 'center', fontSize: 25, fontWeight: 'bold'}}>
                Victoria Robertson
              </Text>
              <Text
                style={{textAlign: 'center', fontSize: 15, fontWeight: 'bold'}}>
                A mantra goes here
              </Text>
            </View>
          </View>
        </View>
        <View style={{paddingHorizontal: 15}}>
          <View style={{marginVertical: 10}}>
            <View
              style={{
                width: '100%',
                height: 50,
                flexDirection: 'row',
                backgroundColor: '#f6f6f6',
                borderRadius: 50,
                padding: 2,
                borderColor: '#dddddd',
                borderWidth: 1,
              }}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'white',
                  borderRadius: 50,
                }}>
                <Text style={{color: '#60b075', fontWeight: 'bold'}}>
                  Posts
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 50,
                }}>
                <Text>Posts</Text>
              </View>
            </View>
          </View>
          <View>
            <PostCard style={{marginVertical: 5}} />
            <PostCard style={{marginVertical: 5}} />
            <PostCard style={{marginVertical: 5}} />
            <PostCard style={{marginVertical: 5}} />
            <PostCard style={{marginVertical: 5}} />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default ProfilePage;
