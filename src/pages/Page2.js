import React from 'react';
import {
  View,
  SafeAreaView,
  ScrollView,
  FlatList,
  Text,
  StyleSheet,
  Image,
  Button,
  Pressable,
  TouchableNativeFeedback,
} from 'react-native';

const img = require('../assets/work.png');
const icon = require('../assets/gear.png');

import CustomTextInput from '../components/CustomTextInput';
const Page2 = ({navigation}) => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <View style={style.container}>
        <View>
          <Text style={style.header}>Sign up</Text>
          <View style={{width: '80%'}}>
            <Text>
              Please registration with email and continue using our app
            </Text>
          </View>
        </View>
        <View style={{marginVertical: 30}}>
          <Text style={[style.text, {textAlign: 'center'}]}>
            Enter via social network
          </Text>
          <View
            style={{
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <View style={{flexDirection: 'row', marginVertical: 10}}>
              <View
                style={{
                  padding: 20,
                  backgroundColor: 'white',
                  borderRadius: 50,
                  elevation: 3,
                  margin: 5,
                }}>
                <Image source={icon} style={{width: 20, height: 20}} />
              </View>
              <View
                style={{
                  padding: 20,
                  backgroundColor: 'white',
                  borderRadius: 50,
                  elevation: 3,
                  margin: 5,
                }}>
                <Image source={icon} style={{width: 20, height: 20}} />
              </View>
            </View>
            <View>
              <Text
                style={[
                  style.text,
                  {
                    color: '#333a4f',
                    textAlign: 'center',
                  },
                ]}>
                or login with email
              </Text>
            </View>
          </View>
          <View style={{marginVertical: 10}}>
            <View style={{marginVertical: 5}}>
              <CustomTextInput />
            </View>
            <View style={{marginVertical: 5}}>
              <CustomTextInput />
            </View>
          </View>
          <View>
            <Text>I agree with privacy policy</Text>
          </View>
        </View>
        <View>
          <View style={{marginVertical: 10}}>
            <TouchableNativeFeedback>
              <View
                style={{
                  padding: 15,
                  backgroundColor: '#747cfb',
                  borderRadius: 10,
                }}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 15,
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  Signup
                </Text>
              </View>
            </TouchableNativeFeedback>
          </View>
          <View style={{marginVertical: 10, flexDirection: 'row'}}>
            <Text
              style={{
                fontWeight: 'bold',
              }}>
              You already have an acocunt?{' '}
            </Text>
            <Pressable onPress={() => navigation.navigate('Page3')}>
              <Text
                style={{
                  color: '#71a3ff',
                  fontWeight: 'bold',
                }}>
                Login
              </Text>
            </Pressable>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const acientColor = '#71a3ff';
const style = StyleSheet.create({
  container: {
    paddingHorizontal: 15,
  },
  header: {
    color: acientColor,
    fontSize: 25,
    fontWeight: 'bold',
    marginVertical: 10,
  },
  text: {
    fontSize: 15,
    color: acientColor,
    fontWeight: 'bold',
  },
});
export default Page2;
