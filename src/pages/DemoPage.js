import React, {useState} from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import ContextMenu from 'react-native-context-menu-view';
const DemoPage = (props) => {
  return (
    <SafeAreaView>
      <View>
        <ContextMenu
          actions={[{title: 'Title 1'}, {title: 'Title 2'}]}
          onPress={(e) => {
            console.warn(
              `Pressed ${e.nativeEvent.name} at index ${e.nativeEvent.index}`,
            );
          }}>
          <View style={{width: 100, height: 100, backgroundColor: 'red'}} />
        </ContextMenu>
      </View>
    </SafeAreaView>
  );
};

export default DemoPage;
