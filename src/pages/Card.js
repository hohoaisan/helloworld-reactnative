import * as React from 'react';
import {Text, View, StyleSheet, Image, SafeAreaView} from 'react-native';
import {IconButton} from 'react-native-paper';

const Card = ({item}) => {
  return (
    <View style={styles.card}>
      <View style={styles.cardHeading}>
        <View style={{flex: 1}}>
          <Image
            source={require('../assets/watame.png')}
            style={{
              width: 50,
              height: 50,
              borderRadius: 50,
            }}
          />
        </View>
        <View style={{alignSelf: 'flex-start'}}>
          <IconButton
            icon="star"
            size={20}
            color="#f74747"
            style={{margin: 0}}
            onPress={() => console.log('Pressed')}
          />
        </View>
      </View>
      <View>
        <Text style={styles.textHeading} numberOfLines={1}>
          {item.name}
        </Text>
        <Text style={styles.textContent} numberOfLines={1}>
          {item.content}
        </Text>
        <Text style={styles.textContent} numberOfLines={1}>
          {item.subcontent}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    padding: 15,
    elevation: 3,
    backgroundColor: 'white',
    borderRadius: 3,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3.84,
    flex: 1,
    margin: 4,
  },
  cardHeading: {
    flexDirection: 'row',
    alignContent: 'flex-start',
    marginBottom: 10,
  },
  textHeading: {
    fontWeight: 'bold',
    color: '#16364e',
  },
  textContent: {
    color: '#97aab3',
    fontSize: 12,
  },
});
export default Card;
