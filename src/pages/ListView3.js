import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  SafeAreaView,
  FlatList,
} from 'react-native';
import {IconButton} from 'react-native-paper';
import Card from './Card';
const data = [
  {
    name: 'Ed revalo',
    content: 'Drivers, New Jersey',
    subcontent: 'G32-3453-321',
  },
  {
    name: 'Ed revalo 2 ',
    content: 'Drivers, New Jersey',
    subcontent: 'G32-3453-321',
  },
  {
    name: 'Ed revalo 3',
    content: 'Drivers, New Jersey',
    subcontent: 'G32-3453-321',
  },
  {
    name: 'Ed revalo 4',
    content: 'Drivers, New Jersey',
    subcontent: 'G32-3453-321',
  },
];
export default function ListView() {
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <View style={styles.listViewControls}>
          <IconButton
            icon="format-list-checkbox"
            size={20}
            color="#16364e"
            style={{margin: 0}}
            onPress={() => console.log('Pressed')}
          />
          <IconButton
            icon="view-grid"
            size={20}
            color="#16364e"
            style={{margin: 0}}
            onPress={() => console.log('Pressed')}
          />
        </View>
        <View>
          <FlatList
            data={data}
            numColumns={2}
            keyExtractor={(item, index) => item.id}
            renderItem={(item) => <Card {...item} />}
          />
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
  },
  listViewControls: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
});
