/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  Image,
  ImageBackground,
  TouchableNativeFeedback,
  FlatList,
  Alert,
  Modal,
  TouchableHighlight,
} from 'react-native';
import {
  Appbar,
  Button,
  Menu,
  Divider,
  Provider,
  Headline,
  IconButton,
} from 'react-native-paper';
import {Container, Header, Content, ActionSheet} from 'native-base';
import CustomTextInput from '../components/CustomTextInput';
const shortid = require('shortid');

const backgroundImage = require('../assets/background.png');
const Card = ({item, onPress, showActionSheet}) => {
  return (
    <View
      style={{
        borderRadius: 10,
        overflow: 'hidden',
        width: '100%',
        height: 80,
      }}>
      <TouchableNativeFeedback onPress={onPress}>
        <View
          style={{
            padding: 10,
            backgroundColor: 'rgba(196, 196, 196, 0.3)',
            width: '100%',
            height: '100%',
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View
              style={{
                height: '100%',
                aspectRatio: 1,
                backgroundColor: 'rgba(0,0,0,0.1)',
                marginRight: 20,
                borderRadius: 100,

                overflow: 'hidden',
              }}>
              <Image
                source={{
                  uri: item.img,
                  width: 100,
                  height: 100,
                }}
                style={{width: '100%', height: '100%'}}
              />
            </View>
            <View style={{flex: 1}}>
              <View>
                <Text style={{color: 'white', fontSize: 18}}>{item.title}</Text>
              </View>
              <View>
                <Text
                  numberOfLines={2}
                  style={{
                    color: 'white',
                  }}>
                  {item.description}
                </Text>
              </View>
            </View>
            <View style={{right: 0}}>
              <IconButton
                icon="dots-vertical"
                color="white"
                size={20}
                style={{padding: 2, margin: 0}}
                onPress={() => showActionSheet(item.id)}
              />
            </View>
          </View>
        </View>
      </TouchableNativeFeedback>
    </View>
  );
};

const GridCell = (WrappedComponent) => {
  return (props) => {
    return (
      <View
        style={{
          width: '100%',
          padding: 5,
        }}>
        <WrappedComponent {...props} />
      </View>
    );
  };
};

const CardWarped = GridCell(Card);

const InputModal = ({modalVisible, setModalVisible, addItem}) => {
  let [title, setTitle] = useState('');
  let [content, setContent] = useState('');
  let handleSubmit = () => {
    if (title && content) {
      addItem(title, content);
    }
    setModalVisible(false);
  };
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        Alert.alert('Modal has been closed.');
      }}>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            backgroundColor: 'white',
            borderRadius: 5,
            margin: 10,
            paddingHorizontal: 10,
          }}>
          <View style={{paddingVertical: 10}}>
            <Text style={{color: 'gray', textAlign: 'center', fontSize: 20}}>
              Add new item
            </Text>
          </View>
          <View style={{paddingVertical: 10}}>
            <CustomTextInput
              label="Title"
              value={title}
              onChangeText={(text) => setTitle(text)}
            />
          </View>
          <View style={{paddingVertical: 10}}>
            <CustomTextInput
              value={content}
              onChangeText={(text) => setContent(text)}
              label="Content"
            />
          </View>
          <View>
            <View style={{paddingVertical: 5}}>
              <Button onPress={handleSubmit}>Submit</Button>
            </View>
            <View style={{paddingVertical: 5}}>
              <Button onPress={() => setModalVisible(false)}>Cancel</Button>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};
let data = [
  {
    id: '1',
    title: 'Facebook',
    img: 'https://picsum.photos/100',
    description: 'Facebook is a social media platform',
  },
  {
    id: '2',
    title: 'Messenger',
    img: 'https://picsum.photos/101',
    description:
      'Messenger is a social media platform mostly focus on texting between people',
  },
  {
    id: '3',
    title: 'Twitter',
    img: 'https://picsum.photos/102',
    description: 'Twitter is a social media platform too',
  },
  {
    id: '4',
    title: 'Code React Native',
    img: 'https://picsum.photos/103',
    description: 'Nothing in particullar',
  },
  {
    id: '5',
    title: 'Not thing important',
    img: 'https://picsum.photos/104',
    description: 'Yes',
  },
];
const ListViewWithContext = (props) => {
  let [menuVisible, setMenuVisible] = useState(false);
  let [modalVisible, setModalVisible] = useState(false);
  let [listItem, setListItem] = useState(data);
  let [selectedId, setSelectedId] = useState(null);
  let openTopMenu = () => setMenuVisible(true);
  let closeTopMenu = () => setMenuVisible(false);
  let addItem = (title, content) => {
    console.log(title, content);
    let newList = listItem.map((item) => ({...item}));
    newList.push({
      id: shortid.generate(),
      title,
      img: 'https://picsum.photos/100',
      description: content,
    });
    setListItem(newList);
  };

  let clearItems = () => {
    setListItem([]);
  };
  let restoreItems = () => {
    setListItem(data);
  };
  let removeItem = (id) => {
    let newList = listItem.map((item) => ({...item}));
    newList = newList.filter((item) => item.id !== id);
    setListItem(newList);
  };
  let options = [
    {text: 'Edit item', action: () => {}},
    {text: 'Delete item', action: (id) => removeItem(id)},
    {text: 'Cancel', action: () => {}},
  ];
  let showActionSheet = (id) => {
    ActionSheet.show(
      {
        options,
        cancelButtonIndex: 2,
        destructiveButtonIndex: 1,
        title: 'Option',
      },
      (buttonIndex) => options[buttonIndex].action(id),
    );
  };
  return (
    <Provider>
      <Appbar
        style={{justifyContent: 'space-between', backgroundColor: 'tomato'}}>
        <View />
        <View>
          <Menu
            visible={menuVisible}
            onDismiss={closeTopMenu}
            anchor={
              <Appbar.Action icon="dots-vertical" onPress={openTopMenu} />
            }>
            <Menu.Item
              onPress={() => setModalVisible(!modalVisible)}
              title="Add new item"
            />
            <Menu.Item onPress={clearItems} title="Remove all items" />
            <Divider />
            <Menu.Item onPress={restoreItems} title="Restore default" />
          </Menu>
        </View>
      </Appbar>
      <InputModal
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
        addItem={addItem}
      />
      <ImageBackground
        source={backgroundImage}
        style={{
          width: '100%',
          height: '100%',
          flex: 1,
        }}>
        <View>
          <View style={{backgroundColor: 'transfarent'}}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                flexWrap: 'wrap',
              }}>
              <FlatList
                data={listItem}
                renderItem={(item) => (
                  <CardWarped {...item} showActionSheet={showActionSheet} />
                )}
                keyExtractor={(item) => item.id}
                extraData={selectedId}
              />
            </View>
          </View>
        </View>
      </ImageBackground>
    </Provider>
  );
};

export default ListViewWithContext;
