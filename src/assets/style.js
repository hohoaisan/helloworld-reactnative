import {StyleSheet} from 'react-native';

const style = StyleSheet.create({
  container: {
    paddingHorizontal: 15,
  },
  headingImg: {
    maxHeight: 100,
    maxWidth: 200,
    marginVertical: 20,
  },
  headingText: {
    fontSize: 50,
    lineHeight: 50,
    fontFamily: 'UVNThanhPhoNang',
  },
  formControls: {
    marginVertical: 5,
  },
  formHelperText: {
    marginVertical: 10,
  },
});

module.exports = style;
