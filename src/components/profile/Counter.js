import * as React from 'react';

import {Text} from 'react-native-paper';
import {StyleSheet, View} from 'react-native';
const Counter = (props) => {
  let {number, unit, bgColor} = props;

  return (
    <View style={{...style.container, backgroundColor: bgColor, flex: 1}}>
      <Text style={style.number}>{number}</Text>
      <Text style={style.unit}>{unit}</Text>
    </View>
  );
};
const style = StyleSheet.create({
  container: {
    padding: 10,
  },
  flexCenter: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  number: {
    textAlign: 'center',
    fontSize: 25,
    textTransform: 'uppercase',
    color: 'white',
  },
  unit: {
    textAlign: 'center',
    fontSize: 15,
    textTransform: 'uppercase',
    color: 'white',
  },
});
export default Counter;
