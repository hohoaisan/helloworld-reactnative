import * as React from 'react';

import {Text, Avatar} from 'react-native-paper';
import {StyleSheet, View} from 'react-native';
const ProfileAvatarDetail = (props) => {
  let {title, subtitle, imgSrc} = props;
  return (
    <View>
      <View style={style.profileImageContainer}>
        <View style={style.profileImage}>
          <Avatar.Image size={100} source={imgSrc} />
        </View>
      </View>
      <Text style={style.title}>{title}</Text>
      <Text style={style.subtitle}>{subtitle}</Text>
    </View>
  );
};
const style = StyleSheet.create({
  flexCenter: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  headingContent: {
    paddingVertical: 40,
  },
  title: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
  },
  subtitle: {
    color: 'gray',
    fontSize: 15,
    textAlign: 'center',
  },
  profileImage: {
    borderWidth: 4,
    borderColor: 'white',
    borderRadius: 52,
  },
  profileImageContainer: {
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 1000,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
});
export default ProfileAvatarDetail;
