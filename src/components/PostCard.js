import React from 'react';
import {View, Image, Text} from 'react-native';
const profile = require('../assets/watame.png');
const PostCard = (props) => {
  return (
    <View {...props}>
      <View
        style={{
          flexDirection: 'row',
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.22,
          shadowRadius: 2.22,
          backgroundColor: 'white',
          elevation: 1,
          justifyContent: 'center',
        }}>
        <View
          style={{
            borderRadius: 5,
            overflow: 'hidden',
            padding: 5,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image source={profile} style={{width: 40, height: 40}} />
        </View>
        <View style={{flexShrink: 1, padding: 5}}>
          <View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontWeight: 'bold'}}>Header</Text>
              <Text style={{color: '#bebebe'}}>8m ago</Text>
            </View>
            <View>
              <Text style={{fontSize: 12}} numberOfLines={2}>
                lorem iblha uahya akjsak uahdua hajshd jkaa iajskd jkajka lorem
                iblha uahya akjsak uahdua hajshd jkaa iajskd jkajka
              </Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default PostCard;
