import * as React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {
  Text,
  StyleSheet,
  View,
  Pressable,
  TouchableNativeFeedback,
} from 'react-native';
const CustomButotnGradient = ({children, colors, start, end, ...rest}) => {
  return (
    <Pressable {...rest}>
      <LinearGradient
        colors={colors}
        style={styles.linearGradient}
        start={start}
        end={end}>
        <Text style={styles.buttonText}>{children}</Text>
      </LinearGradient>
    </Pressable>
  );
};

var styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 500,
  },
  buttonText: {
    fontSize: 15,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
});

CustomButotnGradient.defaultProps = {
  children: 'sample text',
  colors: ['#8BBB43', '#1c8543'],
};
export default CustomButotnGradient;
