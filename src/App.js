import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {Button} from 'react-native-paper';

import LoginView from './pages/Login';
import RegisterView from './pages/Register';
import Profile from './pages/Profile';
import ListView from './pages/ListView';
import GridView from './pages/GridView';
import SignInPage from './pages/SignInPage';
import ProfilePage from './pages/ProfilePage';
// import DemoPage from './pages/DemoPage';
import Page1 from './pages/Page1';
import Page2 from './pages/Page2';
import Page3 from './pages/Page3';
import ListViewWithContextMenu from './pages/ListViewWithContextMenu';
import ListView3 from './pages/ListView3';
const Stack = createStackNavigator();

const HomeScreen = ({navigation}) => {
  return (
    <View style={{margin: 10}}>
      <Button
        mode="contained"
        disabled={false}
        uppercase={true}
        style={{marginBottom: 5}}
        onPress={() => navigation.navigate('Profile')}>
        Profile
      </Button>

      <Button
        mode="contained"
        disabled={false}
        uppercase={true}
        style={{marginBottom: 5}}
        onPress={() => navigation.navigate('Register')}>
        Register
      </Button>
      <Button
        mode="contained"
        disabled={false}
        uppercase={true}
        style={{marginBottom: 5}}
        onPress={() => navigation.navigate('Login')}>
        Login
      </Button>
      <Button
        mode="contained"
        disabled={false}
        uppercase={true}
        style={{marginBottom: 5}}
        onPress={() => navigation.navigate('ListView')}>
        ListView
      </Button>
      <Button
        mode="contained"
        disabled={false}
        uppercase={true}
        style={{marginBottom: 5}}
        onPress={() => navigation.navigate('GridView')}>
        GridView
      </Button>
      <Button
        mode="contained"
        disabled={false}
        uppercase={true}
        style={{marginBottom: 5}}
        onPress={() => navigation.navigate('SignInPage')}>
        Signin Page
      </Button>
      <Button
        mode="contained"
        disabled={false}
        uppercase={true}
        style={{marginBottom: 5}}
        onPress={() => navigation.navigate('ProfilePage')}>
        ProfilePage
      </Button>
      <Button
        mode="contained"
        disabled={false}
        uppercase={true}
        style={{marginBottom: 5}}
        onPress={() => navigation.navigate('DemoPage')}>
        DemoPage
      </Button>
      <Button
        mode="contained"
        disabled={false}
        uppercase={true}
        style={{marginBottom: 5}}
        onPress={() => navigation.navigate('Page1')}>
        Page1
      </Button>
      <Button
        mode="contained"
        disabled={false}
        uppercase={true}
        style={{marginBottom: 5}}
        onPress={() => navigation.navigate('Page2')}>
        Page2
      </Button>
      <Button
        mode="contained"
        disabled={false}
        uppercase={true}
        style={{marginBottom: 5}}
        onPress={() => navigation.navigate('Page3')}>
        Page3
      </Button>
      <Button
        mode="contained"
        disabled={false}
        uppercase={true}
        style={{marginBottom: 5}}
        onPress={() => navigation.navigate('ListViewWithContextMenu')}>
        ListViewWithContextMenu
      </Button>
      <Button
        mode="contained"
        disabled={false}
        uppercase={true}
        style={{marginBottom: 5}}
        onPress={() => navigation.navigate('ListView3')}>
        ListView3
      </Button>
    </View>
  );
};
const App = () => {
  return (
    <NavigationContainer>
      <StatusBar barStyle="dark-content" />
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Profile" component={Profile} />
        <Stack.Screen name="Register" component={RegisterView} />
        <Stack.Screen name="Login" component={LoginView} />
        <Stack.Screen name="ListView" component={ListView} />
        <Stack.Screen name="GridView" component={GridView} />
        <Stack.Screen name="SignInPage" component={SignInPage} />
        <Stack.Screen name="ProfilePage" component={ProfilePage} />
        <Stack.Screen name="Page1" component={Page1} />
        <Stack.Screen name="Page2" component={Page2} />
        <Stack.Screen name="Page3" component={Page3} />
        <Stack.Screen
          name="ListViewWithContextMenu"
          component={ListViewWithContextMenu}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen name="ListView3" component={ListView3} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
