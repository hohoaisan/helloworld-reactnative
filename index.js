/**
 * @format
 */
import React from 'react';
import {AppRegistry} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {Container, Root} from 'native-base';
const theme = {
  ...DefaultTheme,
  roundness: 2,
  dark: false,
};
export default function Main() {
  return (
    <Container>
      <PaperProvider theme={theme}>
        <Root>
          <App />
        </Root>
      </PaperProvider>
    </Container>
  );
}

AppRegistry.registerComponent(appName, () => Main);
